# Pinecone Recommendation Engine

This project is a demonstration of constructing a recommendation engine with Pinecone. Pinecone is a easy to use vector database that allows the storage of indices for vector search use cases. In this instance, we use a data set on cocktails, utilize a BERT pre-trained model to tokenize and embed the ingredients of each cocktail into an associated vector. We then push this data in a json payload to the Pinecone vector database. The ingestion syntax is fairly easy as long as the json file is formatted correctly:
![alt text](images/pineupload.png)

The final data looks like this for each index:
 ![alt text](images/vector.png)

We can then make simple query functions which call the Pinecone API and return desired numbers of similar cocktails:
![alt text](images/query.png)

An additional feature of Pinecone is that it provides a serverless option, where you pay for what you use. In this demo I have also integrated Pinecone with my AWS account via the marketplace, which should allow for the easy monitoring and integration with my other resources.
![alt text](images/pineconedash.png)

Pinecone also provides usage monitoring for free which is a nice added benefit:
![alt text](images/pineinstrument.png)

Once we have our data loaded into Pinecone we can execute our query functions to get easy visualizations for our results:
![alt text](images/top1.png)
![alt text](images/top2.png)

Because this works by calling the Pinecone API, we can bake this into a lambda function or any webservice backend with relative ease. This project highlights the power of Pinecone Serverless as a recommendation engine.